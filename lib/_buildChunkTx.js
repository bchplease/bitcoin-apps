/**
 * Build Chunk Transaction
 */
const buildChunkTx = function (_config) {
    console.log('\nbuildChunkTx (config):', _config)

    /* Initialize transaction builder. */
    let transactionBuilder = null

    /* Handle network. */
    transactionBuilder = new this.bitbox.TransactionBuilder('bitcoincash')

    /* Add input. */
    transactionBuilder.addInput(
        _config.input_utxo.txid,
        _config.input_utxo.vout
    )

    /* Calculate chunk transaction fee. */
    let chunkTxFee = this.utils.calculateDataChunkMinerFee(
        _config.bfpChunkOpReturn.length)

    /* Calculate output amount. */
    let outputAmount = _config.input_utxo.satoshis - chunkTxFee

    /* Add output. */
    // NOTE: Chunk OP_RETURN
    transactionBuilder.addOutput(_config.bfpChunkOpReturn, 0)

    /* Add output. */
    // NOTE: Genesis token mint
    transactionBuilder.addOutput(_config.input_utxo.address, outputAmount)

    // sign inputs

    const paymentKeyPair = this.bitbox.ECPair
        .fromWIF(_config.input_utxo.wif)

    /* Sign transaction. */
    transactionBuilder.sign(
        0,
        paymentKeyPair,
        null,
        transactionBuilder.hashTypes.SIGHASH_ALL,
        _config.input_utxo.satoshis
    )

    /* Return "built" transaction. */
    return transactionBuilder.build()
}

/* Export module. */
module.exports = buildChunkTx
