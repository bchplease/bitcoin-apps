/* Import modules. */
const BfpUtils = require('./utils')

/**
 * Upload Hash Only
 */
const uploadHashOnlyObject = async function (
    _type,                   // file = 1, folder = 3
    _fundingUtxo,            // object in form: { txid:'', satoshis:#, vout:# }
    _fundingAddress,         // string
    _fundingWif,             // hex string?
    _objectDataArrayBuffer,  // ArrayBuffer
    _objectName,             // string
    _objectExt,              // string
    _prevObjectSha256Hex,    // hex string
    _objectExternalUri,      // utf8 string
    _objectReceiverAddress,  // string
    _signProgressCallback,
    _signFinishedCallback,
    _uploadProgressCallback,
    _uploadFinishedCallback
) {
    /* Set file size. */
    console.log('\n_objectReceiverAddress', _objectReceiverAddress)
    console.log('\n_objectDataArrayBuffer', _objectDataArrayBuffer)

    /* Set file size. */
    const fileSize = _objectDataArrayBuffer.byteLength

    /* Calculate hash. */
    const hash = this.bitbox.Crypto
        .sha256(Buffer.from(_objectDataArrayBuffer))
        .toString('hex')

    /* Initialize chunk count. */
    let chunkCount = 0 //Math.floor(fileSize / 220);

    // estimate cost
    // build empty meta data OpReturn
    const configEmptyMetaOpReturn = {
        msgType: _type,
        chunkCount,
        fileName: _objectName,
        fileExt: _objectExt,
        fileSize,
        fileSha256Hex: hash,
        prevFileSha256Hex: _prevObjectSha256Hex,
        fileUri: _objectExternalUri
    }
    console.log('\nconfigEmptyMetaOpReturn', configEmptyMetaOpReturn)

    //* ** building transaction
    const transactions = []
    const txid = _fundingUtxo.txid
    const satoshis = _fundingUtxo.satoshis
    const vout = _fundingUtxo.vout

    const metaOpReturn = BfpUtils.buildMetadataOpReturn(configEmptyMetaOpReturn)
    console.log('\nmetaOpReturn', metaOpReturn)

    // build meta data transaction
    const configMetaTx = {
        bfpMetadataOpReturn: metaOpReturn,
        input_utxo: {
            txid, //chunksTx.getId(),
            vout,
            satoshis, //chunksTx.outs[1].value,
            wif: _fundingWif
        },
        fileReceiverAddress: (_objectReceiverAddress !== null)
            ? _objectReceiverAddress : _fundingAddress
    }

    /* Build metadata transaction. */
    const metaTx = require('./_buildMetadataTx')(configMetaTx)
    // console.log('\nMeta transaction', metaTx)

    /* Add to transactions. */
    transactions.push(metaTx)

    /* Validate sign progress */
    if (_signProgressCallback !== null) {
        _signProgressCallback(100)
    }

    /* Validate finished progress. */
    // NOTE: signing finished
    if (_signFinishedCallback !== null) {
        _signFinishedCallback()
    }

    /* Validate upload progress. */
    // NOTE: sending transaction
    if (_uploadProgressCallback !== null) {
        _uploadProgressCallback(0)
    }

    console.log('\nTransaction #0: ', transactions[0].toHex())

    /* Request transaction id. */
    let bfTxid = await this.network
        .sendTxWithRetry(transactions[0].toHex())
        .catch(err => console.error(err))

    // progress
    if (_uploadProgressCallback!==null) {
        _uploadProgressCallback(100)
    }

    /* Add prefix. */
    if (bfTxid) {
        bfTxid = 'bitcoinfile:' + bfTxid
    }

    /* Validate finished progress. */
    if (_uploadFinishedCallback !== null) {
        _uploadFinishedCallback(bfTxid)
    }

    /* Return transaction id. */
    return bfTxid
}

/* Export module. */
module.exports = uploadHashOnlyObject
