#!/usr/bin/env node

/* Import module.s */
const Bfp = require('../index').bfp
const fs = require('fs')

/* Initialize BFP. */
const bfp = new Bfp()

;(async function () {
    // const target = 'bitcoinfile:7e4600323c934926369c136562f5483e3df79baf087c8dd2b0ed1aea69d5ee49' // NOTE: mario.png
    // const target = 'bitcoinfile:db59d0473c02ff0abd10473acb58b778328efc713d6c7468594310dde5dfdd8a' // NOTE: bitcoin-icon-small.png
    // const target = 'bitcoinfile:d349f2839c116b89425c03a3fd017e175f3e100aaaa32f5f0d90df8ae9db39e2' // NOTE: index.html
    // const target = 'bitcoinfile:d0924594de9c419b968918ed0c4cf815837505146a5a9f3cbdacaa6f0a00e3d0' // NOTE: index.bml
    const target = 'bitcoinfile:0a2ea675193a3011799d2c466db7bc1b9490453034c2e98b0399be4b52f0bb0d' // NOTE: tenk.bin

    // 1 - download file using URI
    const result = await bfp.downloadFile(target)
    console.log('\nDownload complete.')

    // Wait for download to complete -- Mario.png TAKES ABOUT 6 SEC TO DOWNLOAD!

    // 2 - result includes a boolean check telling you if the file's sha256 matches the file's metadata```
    if (result.passesHashCheck) {
        console.log(`\nSuccess: downloaded file sha256 matches file's metadata`)
    }

    // 3 - do something with the file...
    const fileBuffer = result.fileBuf
    const filename = result.filename
    const fileext = result.fileext
    console.log('\nFile result:', result)
    console.log('\nFile buffer:', fileBuffer)

    const buf = Buffer.from(fileBuffer, 'base64')
    fs.writeFile(`${filename}${fileext}`, buf, 'base64', () => {
        console.log('file written!')
    })
})()
